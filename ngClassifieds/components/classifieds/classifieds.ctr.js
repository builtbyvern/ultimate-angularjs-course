(function(){

	"use strict"; 

	angular
		.module('ngClassifieds')
		.controller('classifiedsCtrl', function($scope, $state, $http, classifiedsFactory, $mdSidenav, $mdToast, $mdDialog, $stateParams) {

			// vm for view model
			var vm = this;

			vm.openSidebar = openSidebar;
			vm.closeSidebar = closeSidebar;
			vm.saveClassified = saveClassified;
			vm.editClassified = editClassified;
			vm.saveEdit = saveEdit;
			vm.deleteClassified = deleteClassified;

			vm.classifieds = classifiedsFactory.ref;
			vm.classifieds.$loaded().then(function(classifieds) {
				vm.categories = getCategories(classifieds);
			});
			vm.categories;
			vm.editing;
			vm.classified;


			// classifiedsFactory.getClassifieds().then(function(classifieds) {
			// 	vm.classifieds = classifieds.data;
			// 	vm.categories = getCategories(vm.classifieds);
			// });


			// quick api example
			$http.get('https://api.github.com/users').then(function(response){
				console.log(response);
			});

			$scope.$on('newClassified', function(event, classified) {
				// classified.id = vm.classifieds.length + 1;
				// vm.classifieds.push(classified);
				vm.classifieds.$add(classified);
				showToast('Classified Saved');
			});

			$scope.$on('editSaved', function(event, message){
				showToast(message);
			});

			var contact = {
				name: "Vern",
				phone: '208.283.6343',
				email: 'vernworldwide@gmail.com'
			}

			

			function openSidebar() {
				$state.go('classifieds.new');
			}

			function closeSidebar() {
				$mdSidenav('left').close();
			}

			function saveClassified(classified) {
				if (classified) {
					classified.contact = contact;
					vm.classifieds.push(classified);
					vm.classified = {};
					closeSidebar();
					showToast('Classified Saved!');
				}
			}

			function editClassified(classified) {
				$state.go('classifieds.edit', {
					id: classified.$id,
					// classified: classified
				});
			}

			function saveEdit(classified) {
				vm.editing = false;
				vm.classified = {};
				closeSidebar();
				showToast("Edit Saved!");
			}

			function deleteClassified(event, classified) {
				var confirm = $mdDialog.confirm()
					.title('Are your sure you want to delete ' +  classified.title + '?')
					.ok("Yep")
					.cancel('Nope').
					targetEvent(event);
				$mdDialog.show(confirm).then(function(){
					vm.classifieds.$remove(classified);
					showToast('It done been deleted!');
				}, function(){

				});
				
				
			}

			function showToast(message){
				$mdToast.show(
					$mdToast.simple()
						.content(message)
						.position('top, right')
						.hideDelay(3000)
				);
			}

			function getCategories(classifieds) {
				var categories = [];

				angular.forEach(classifieds, function(item) {
					angular.forEach(item.categories, function(category){
						categories.push(category);
					});
				});

				return _.uniq(categories);
			}

			
		});

})();