(function() {
	"use strict";

	angular
		.module("ngClassifieds")
		.directive("classifiedCard", function(){
			return  {
				templateUrl: "components/classifieds/card/classified-card.tpl.html",
				controller: classifiedCardController,
				controllerAs: "vm",
				scope: {
					classifieds: "=classifieds",
					classifiedsFilter : "=classifiedsFilter",
					category: "=category"
				},
						 
			}

			function classifiedCardController($state, $scope, $mdDialog) {

				var vm = this;
				vm.editClassified = editClassified;
				vm.deleteClassified = deleteClassified;
	
				function editClassified(classified) {
					$state.go('classifieds.edit', {
						id: classified.$id,
						// classified: classified
					});
				}

				function deleteClassified(event, classified) {
					var confirm = $mdDialog.confirm()
						.title('Are your sure you want to delete ' +  classified.title + '?')
						.ok("Yep")
						.cancel('Nope').
						targetEvent(event);
					$mdDialog.show(confirm).then(function(){
						vm.classifieds.$remove(classified);
						showToast('It done been deleted!');
					}, function(){

					});				
				}

				function showToast(message){
					$mdToast.show(
						$mdToast.simple()
							.content(message)
							.position('top, right')
							.hideDelay(3000)
					);
				}				
			}
		});

})();